<?php
/**
 * @file
 * contento_field_categories.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function contento_field_categories_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add terms in categories'.
  $permissions['add terms in categories'] = array(
    'name' => 'add terms in categories',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomyplus',
  );

  // Exported permission: 'delete terms in categories'.
  $permissions['delete terms in categories'] = array(
    'name' => 'delete terms in categories',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in categories'.
  $permissions['edit terms in categories'] = array(
    'name' => 'edit terms in categories',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'list terms in categories'.
  $permissions['list terms in categories'] = array(
    'name' => 'list terms in categories',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomyplus',
  );

  return $permissions;
}

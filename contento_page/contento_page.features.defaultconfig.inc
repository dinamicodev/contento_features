<?php
/**
 * @file
 * contento_page.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function contento_page_defaultconfig_features() {
  return array(
    'contento_page' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function contento_page_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create page content'.
  $permissions['create page content'] = array(
    'name' => 'create page content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any page content'.
  $permissions['delete any page content'] = array(
    'name' => 'delete any page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own page content'.
  $permissions['delete own page content'] = array(
    'name' => 'delete own page content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any page content'.
  $permissions['edit any page content'] = array(
    'name' => 'edit any page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own page content'.
  $permissions['edit own page content'] = array(
    'name' => 'edit own page content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view page content'.
  $permissions['view page content'] = array(
    'name' => 'view page content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'nodeplus',
  );

  return $permissions;
}

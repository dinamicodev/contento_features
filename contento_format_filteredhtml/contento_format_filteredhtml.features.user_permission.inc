<?php
/**
 * @file
 * contento_format_filteredhtml.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function contento_format_filteredhtml_user_default_permissions() {
  $permissions = array();

  // Exported permission: use text format filtered_html.
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'filter',
  );

  return $permissions;
}

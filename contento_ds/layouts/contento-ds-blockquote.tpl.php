<?php
/**
 * @file
 * Display Suite Article template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $header: Rendered content for the "Header" region.
 * - $header_classes: String of classes that can be used to style
 *     the "Header" region.
 *
 * - $figure: Rendered content for the "Figure" region.
 * - $figure_classes: String of classes that can be used to style
 *     the "Figure" region.
 *
 * - $section: Rendered content for the "Content" region.
 * - $section_classes: String of classes that can be used to style
 *     the "Content" region.
 *
 * - $aside: Rendered content for the "Content" region.
 * - $aside_classes: String of classes that can be used to style
 *     the "Content" region.
 *
 * - $footer: Rendered content for the "Footer " region.
 * - $footer_classes: String of classes that can be used to style
 *     the "Footer " region.
 */

$classes = !empty($classes) ? ' class="' . $classes . '"' : '';
$header_classes = !empty($header_classes) ? ' class="' . $header_classes . '"' : '';
$footer_classes = !empty($footer_classes) ? ' class="' . $footer_classes . '"' : '';
?>
<blockquote<?php print $classes; ?>>

<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php if ($header): ?>
<header<?php print $header_classes; ?>>
<?php print $header; ?>
</header>
<?php endif; ?>

<?php if ($ds_content): ?>
<?php print $ds_content; ?>
<?php endif; ?>

<?php if ($footer): ?>
<footer<?php print $footer_classes; ?>>
<?php print $footer; ?>
</footer>
<?php endif; ?>

</blockquote>

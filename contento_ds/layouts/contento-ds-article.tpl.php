<?php
$classes = !empty($classes) ? ' class="' . trim($classes) . '"' : '';
$header_classes = !empty($header_classes) ? ' class="' . trim($header_classes) . '"' : '';
$ds_content_classes = !empty($ds_content_classes) ? ' class="' . trim($ds_content_classes) . '"' : '';
#$hgroup_classes = !empty($hgroup_classes) ? ' class="' . $hgroup_classes . '"' : '';
#$figure_classes = !empty($figure_classes) ? ' class="' . $figure_classes . '"' : '';
$aside_classes = !empty($aside_classes) ? ' class="' . trim($aside_classes) . '"' : '';
$footer_classes = !empty($footer_classes) ? ' class="' . trim($footer_classes) . '"' : '';
?>
<<?php print $layout_wrapper; print $classes; print $layout_attributes; ?>>

<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php if ($header): ?>
<<?php print $header_wrapper; print $header_classes; ?>>
<?php print $header; ?>
</<?php print $header_wrapper; ?>>
<?php endif; ?>

<?php if ($ds_content): ?>
<?php if($ds_content_wrapper) : print '<' . $ds_content_wrapper . $ds_content_classes . ">\n"; endif; ?>
<?php print $ds_content; ?>
<?php if($ds_content_wrapper) : print '</' . $ds_content_wrapper . ">\n"; endif; ?>
<?php endif; ?>

<?php if ($aside): ?>
<<?php print $aside_wrapper; print $aside_classes; ?>>
<?php print $aside; ?>
</<?php print $aside_wrapper; ?>>
<?php endif; ?>

<?php if ($footer): ?>
<<?php print $footer_wrapper; print $footer_classes; ?>>
<?php print $footer; ?>
</<?php print $footer_wrapper; ?>>
<?php endif; ?>

</<?php print $layout_wrapper; ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>

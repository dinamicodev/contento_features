<?php
/**
 * @file
 * contento_format_fullhtml_ck.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function contento_format_fullhtml_ck_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use text format full_html'.
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'filter',
  );

  return $permissions;
}

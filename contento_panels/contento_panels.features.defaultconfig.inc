<?php
/**
 * @file
 * contento_panels.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function contento_panels_defaultconfig_features() {
  return array(
    'contento_panels' => array(
      'strongarm' => 'strongarm',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function contento_panels_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panopoly_magic_show_panels_styles';
  $strongarm->value = 1;
  $export['panopoly_magic_show_panels_styles'] = $strongarm;

  return $export;
}

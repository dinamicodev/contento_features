<?php
/**
 * @file
 * contento_news.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function contento_news_defaultconfig_features() {
  return array(
    'contento_news' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function contento_news_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create article content'.
  $permissions['create article content'] = array(
    'name' => 'create article content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any article content'.
  $permissions['delete any article content'] = array(
    'name' => 'delete any article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own article content'.
  $permissions['delete own article content'] = array(
    'name' => 'delete own article content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any article content'.
  $permissions['edit any article content'] = array(
    'name' => 'edit any article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own article content'.
  $permissions['edit own article content'] = array(
    'name' => 'edit own article content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view article content'.
  $permissions['view article content'] = array(
    'name' => 'view article content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'nodeplus',
  );

  return $permissions;
}

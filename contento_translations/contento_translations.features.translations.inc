<?php
/**
 * @file
 * contento_translations.features.translations.inc
 */

/**
 * Implements hook_translations_defaults().
 */
function contento_translations_translations_defaults() {
  $translations = array();
  $translations['sl:field']['0f8e5e65822d66133035729e4a9ab903'] = array(
    'source' => 'Event date/location',
    'context' => 'field_date_location:event:label',
    'location' => 'field:field_date_location:event:label',
    'translation' => 'Dogodek datum/lokacija',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['15bbb9d0bbf25e8d2978de1168c749dc'] = array(
    'source' => 'Website',
    'context' => 'field_website:sport_club:label',
    'location' => 'field:field_website:sport_club:label',
    'translation' => 'Spletno mesto',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['189f63f277cd73395561651753563065'] = array(
    'source' => 'Tags',
    'context' => 'field_tags:sport_result:label',
    'location' => 'field:field_tags:sport_result:label',
    'translation' => 'Oznake',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['278a7c0d9bd6e814a84fe32ec0692936'] = array(
    'source' => 'Functionaries',
    'context' => 'field_functionaries:sport_club:label',
    'location' => 'field:field_functionaries:sport_club:label',
    'translation' => 'Funkcionarji',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['359ff4ae83f885f7d3227a4353ea525e'] = array(
    'source' => 'Event date',
    'context' => 'field_event_date:field_date_location:label',
    'location' => 'field:field_event_date:field_date_location:label',
    'translation' => 'Datum dogodka',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['52beb68a9235031e0c52fcdca6a5e692'] = array(
    'source' => 'Featured image',
    'context' => 'field_image:sport_result:label',
    'location' => 'field:field_image:sport_result:label',
    'translation' => 'Izpostavljena slika',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['7e2708aeb65763c54052f57ed1a1ec1d'] = array(
    'source' => 'Attachments',
    'context' => 'field_attachments:sport_result:label',
    'location' => 'field:field_attachments:sport_result:label',
    'translation' => 'Priponke',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['87d17f4624a514e81dc7c8e016a7405c'] = array(
    'source' => 'Mobile',
    'context' => 'field_mobile:sport_club:label',
    'location' => 'field:field_mobile:sport_club:label',
    'translation' => 'Mobilna',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['9810aa2b9f44401be4bf73188ef2b67d'] = array(
    'source' => 'Fax',
    'context' => 'field_fax:sport_club:label',
    'location' => 'field:field_fax:sport_club:label',
    'translation' => 'Faks',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['ac101b32dda4448cf13a93fe283dddd8'] = array(
    'source' => 'Body',
    'context' => 'body:sport_result:label',
    'location' => 'field:body:sport_result:label',
    'translation' => 'Telo',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['af1b98adf7f686b84cd0b443e022b7a0'] = array(
    'source' => 'Categories',
    'context' => 'field_categories:sport_result:label',
    'location' => 'field:field_categories:sport_result:label',
    'translation' => 'Kategorije',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['b5a7adde1af5c87d7fd797b6245c2a39'] = array(
    'source' => 'Description',
    'context' => 'body:event:label',
    'location' => 'field:body:event:label',
    'translation' => 'Opis',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['bcc254b55c4a1babdf1dcb82c207506b'] = array(
    'source' => 'Phone',
    'context' => 'field_phone:sport_club:label',
    'location' => 'field:field_phone:sport_club:label',
    'translation' => 'Telefon',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['ce8ae9da5b7cd6c3df2929543a9af92d'] = array(
    'source' => 'Email',
    'context' => 'field_email:sport_club:label',
    'location' => 'field:field_email:sport_club:label',
    'translation' => 'E-pošta',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['d66a3aef86f498a39569f31178a2c47c'] = array(
    'source' => 'Related content',
    'context' => 'field_related_content:sport_result:label',
    'location' => 'field:field_related_content:sport_result:label',
    'translation' => 'Sorodna vsebina',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['dd7bf230fde8d4836917806aff6a6b27'] = array(
    'source' => 'Address',
    'context' => 'field_address:sport_club:label',
    'location' => 'field:field_address:sport_club:label',
    'translation' => 'Naslov',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['ec0f66006fd8234fcb42214dc4f85557'] = array(
    'source' => 'Event location',
    'context' => 'field_event_location:field_date_location:label',
    'location' => 'field:field_event_location:field_date_location:label',
    'translation' => 'Lokacija dogodka',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['f15cb069d0acf1b74a62f47d053a3c77'] = array(
    'source' => 'Sport disciplines',
    'context' => 'field_sport_disciplines:sport_club:label',
    'location' => 'field:field_sport_disciplines:sport_club:label',
    'translation' => 'Športne discipline',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:field']['fff0d600f8a0b5e19e88bfb821dd1157'] = array(
    'source' => 'Images',
    'context' => 'field_images:sport_club:label',
    'location' => 'field:field_images:sport_club:label',
    'translation' => 'Slike',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:menu']['330f49df8243756a8a4dc7f7f7ee6dfe'] = array(
    'source' => 'Development',
    'context' => 'menu:devel:title',
    'location' => 'menu:menu:devel:title',
    'translation' => 'Razvoj',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:menu']['4d7a3b1c46041dc73dec5764f3a6b63a'] = array(
    'source' => 'Main menu',
    'context' => 'menu:main-menu:title',
    'location' => 'menu:menu:main-menu:title',
    'translation' => 'Glavni meni',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:menu']['846495f9ceed11accf8879f555936a7d'] = array(
    'source' => 'Navigation',
    'context' => 'menu:navigation:title',
    'location' => 'menu:menu:navigation:title',
    'translation' => 'Navigacija',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:menu']['8c41024702b73fabb6d703bd6da3a981'] = array(
    'source' => 'User menu',
    'context' => 'menu:user-menu:title',
    'location' => 'menu:menu:user-menu:title',
    'translation' => 'Uporabniški meni',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:menu']['fe4dbcab9b910577e5035e97ac068dae'] = array(
    'source' => 'Management',
    'context' => 'menu:management:title',
    'location' => 'menu:menu:management:title',
    'translation' => 'Upravljanje',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:taxonomy']['189f63f277cd73395561651753563065'] = array(
    'source' => 'Tags',
    'context' => 'vocabulary:3:name',
    'location' => 'taxonomy:vocabulary:3:name',
    'translation' => 'Oznake',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:taxonomy']['af1b98adf7f686b84cd0b443e022b7a0'] = array(
    'source' => 'Categories',
    'context' => 'vocabulary:2:name',
    'location' => 'taxonomy:vocabulary:2:name',
    'translation' => 'Kategorije',
    'plid' => 0,
    'plural' => 0,
  );
  $translations['sl:taxonomy']['f15cb069d0acf1b74a62f47d053a3c77'] = array(
    'source' => 'Sport disciplines',
    'context' => 'vocabulary:4:name',
    'location' => 'taxonomy:vocabulary:4:name',
    'translation' => 'Športne discipline',
    'plid' => 0,
    'plural' => 0,
  );
  return $translations;
}

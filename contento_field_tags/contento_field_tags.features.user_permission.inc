<?php
/**
 * @file
 * contento_field_tags.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function contento_field_tags_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add terms in tags'.
  $permissions['add terms in tags'] = array(
    'name' => 'add terms in tags',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomyplus',
  );

  // Exported permission: 'delete terms in tags'.
  $permissions['delete terms in tags'] = array(
    'name' => 'delete terms in tags',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in tags'.
  $permissions['edit terms in tags'] = array(
    'name' => 'edit terms in tags',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'list terms in tags'.
  $permissions['list terms in tags'] = array(
    'name' => 'list terms in tags',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor plus' => 'editor plus',
    ),
    'module' => 'taxonomyplus',
  );

  return $permissions;
}

<?php
/**
 * @file
 * contento_field_tags.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function contento_field_tags_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomyplus_vocabulary_tags_empty_text';
  $strongarm->value = 'There is currently no content classified with this term.';
  $export['taxonomyplus_vocabulary_tags_empty_text'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomyplus_vocabulary_tags_node_sort';
  $strongarm->value = 't.created DESC';
  $export['taxonomyplus_vocabulary_tags_node_sort'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomyplus_vocabulary_tags_pager';
  $strongarm->value = '10';
  $export['taxonomyplus_vocabulary_tags_pager'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomyplus_vocabulary_tags_show_rss';
  $strongarm->value = 1;
  $export['taxonomyplus_vocabulary_tags_show_rss'] = $strongarm;

  return $export;
}

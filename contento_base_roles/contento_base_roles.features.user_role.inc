<?php
/**
 * @file
 * contento_base_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function contento_base_roles_user_default_roles() {
  $roles = array();

  // Exported role: contributor.
  $roles['contributor'] = array(
    'name' => 'contributor',
    'weight' => 3,
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 4,
  );

  // Exported role: editor plus.
  $roles['editor plus'] = array(
    'name' => 'editor plus',
    'weight' => 5,
  );

  return $roles;
}
